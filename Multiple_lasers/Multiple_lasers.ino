#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
VL53L0X sensor2;
VL53L0X sensor3;
VL53L0X sensor4;


void setup()
{

  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  
  delay(500);
  
  Wire.begin();


  Serial.begin (9600);

  pinMode(4, INPUT);
  delay(150);
  Serial.println("00");
  sensor.init(true);
  Serial.println("01");
  delay(100);
  sensor.setAddress((uint8_t)22);
  Serial.println("02");

  pinMode(5, INPUT);
    delay(150);
   Serial.println("03");
  sensor2.init(true);
  Serial.println("04");
  delay(100);
  sensor2.setAddress((uint8_t)25);
  Serial.println("05");

  pinMode(6, INPUT);
  delay(150);
  Serial.println("06");
  sensor3.init(true);
  Serial.println("07");
  delay(100);
  sensor3.setAddress((uint8_t)28);
  Serial.println("08");

  pinMode(7, INPUT);
  delay(150);
  Serial.println("09");
  sensor4.init(true);
  Serial.println("10");
  delay(100);
  sensor4.setAddress((uint8_t)31);
  Serial.println("11");

  Serial.println("addresses set");

  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  for (byte i = 1; i < 120; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");

  delay(3000);


  sensor.setTimeout(500);
  sensor2.setTimeout(500);
  sensor3.setTimeout(500);
  sensor4.setTimeout(500);

  
  sensor.startContinuous();
  sensor2.startContinuous();
  sensor3.startContinuous();
  sensor4.startContinuous();

  
}

void loop()
{
  Serial.print("sensor1=");
  Serial.print(sensor.readRangeContinuousMillimeters());
  if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();
  
 

  Serial.print("sensor2=");
  Serial.print(sensor2.readRangeContinuousMillimeters());
  if (sensor2.timeoutOccurred()) { Serial.print(" TIMEOUT"); }


  Serial.println();
  
  Serial.print("sensor3=");
  Serial.print(sensor3.readRangeContinuousMillimeters());
  if (sensor3.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  Serial.println();

  Serial.print("sensor4=");
  Serial.print(sensor4.readRangeContinuousMillimeters());
  if (sensor4.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

  delay(500);

  Serial.println();
}
